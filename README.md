# Moving in a Glance

Moving is stressful. Trying to decide where to move even more so. 
This project is designed to give people a one stop show to get an overview of their potential living area.